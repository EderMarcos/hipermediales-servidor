export const success = (res, status) => (entity) => {
  if (entity) {
    res.status(status || 200).json(entity)
  }
  return null
}

export const notFound = (res) => (entity) => {
  if (entity) {
    return entity
  }
  res.status(404).end()
  return null
}

export const admin = (res, user) => (entity) => {
  if (entity) {
    const isAdmin = user.role === 'admin'
    if (isAdmin) {
      return entity
    }
    res.status(401).end()
  }
  return null
}

export const uniqueValidation = (field, res, next) => err => {
  if (err.name === 'MongoError' && err.code === 11000) {
    res.status(409).json({
      valid: false,
      param: field,
      message: `${field} already registered`
    })
  } else {
    next(err)
  }
}
