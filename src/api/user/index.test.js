import request from 'supertest-as-promised'
import { masterKey } from '../../config'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import routes, { User } from '.'

const app = () => express(routes)

let userSession, user, admin, adminSession

beforeEach(inject(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'b@b.com', password: '123456', role: 'admin' })

  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
}))

test('POST /users 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ name: 'test', lastName: 'test', email: 'test', password: 'test', avatarUrl: 'test', phone: 'test', birthdate: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.password).toEqual('test')
  expect(body.avatarUrl).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.birthdate).toEqual('test')
})

test('POST /users 409 (user) - duplicated email', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ name: 'test', lastName: 'test', email: 'test', password: 'test', avatarUrl: 'test', phone: 'test', birthdate: 'test' })
  expect(status).toBe(409)
  expect(typeof body).toEqual('object')
  expect(body.name).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.password).toEqual('test')
  expect(body.avatarUrl).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.birthdate).toEqual('test')
})

test('GET /users 200 (user)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /users 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /users/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`/${user.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(user.id)
})

test('GET /users/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${user.id}`)
  expect(status).toBe(401)
})

test('GET /users/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /users/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${user.id}`)
    .send({ access_token: userSession, name: 'test', lastName: 'test', email: 'test', password: 'test', avatarUrl: 'test', phone: 'test', birthdate: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(user.id)
  expect(body.name).toEqual('test')
  expect(body.lastName).toEqual('test')
  expect(body.email).toEqual('test')
  expect(body.password).toEqual('test')
  expect(body.avatarUrl).toEqual('test')
  expect(body.phone).toEqual('test')
  expect(body.birthdate).toEqual('test')
})

test('PUT /users/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${user.id}`)
  expect(status).toBe(401)
})

test('PUT /users/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, name: 'test', lastName: 'test', email: 'test', password: 'test', avatarUrl: 'test', phone: 'test', birthdate: 'test' })
  expect(status).toBe(404)
})

test('DELETE /users/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${user.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /users/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${user.id}`)
  expect(status).toBe(401)
})

test('DELETE /users/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
