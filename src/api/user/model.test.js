import crypto from 'crypto'
import { User } from '.'

let user

beforeEach(async () => {
  user = await User.create({ name: 'test', lastName: 'test', email: 'test', password: 'test', avatarUrl: 'test', phone: 'test', birthdate: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = user.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(user.id)
    expect(view.name).toBe(user.name)
    expect(view.lastName).toBe(user.lastName)
    expect(view.email).toBe(user.email)
    expect(view.password).toBe(user.password)
    expect(view.avatarUrl).toBe(user.avatarUrl)
    expect(view.phone).toBe(user.phone)
    expect(view.birthdate).toBe(user.birthdate)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = user.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(user.id)
    expect(view.name).toBe(user.name)
    expect(view.lastName).toBe(user.lastName)
    expect(view.email).toBe(user.email)
    expect(view.password).toBe(user.password)
    expect(view.avatarUrl).toBe(user.avatarUrl)
    expect(view.phone).toBe(user.phone)
    expect(view.birthdate).toBe(user.birthdate)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
