import mongoose, { Schema } from 'mongoose'

const paymentSchema = new Schema({
  idea: {
    type: Schema.ObjectId,
    ref: 'Idea',
    required: true
  },
  title: {
    type: String,
    required: true
  },
  coins: {
    type: Number,
    required: true
  },
  currency: {
    type: String,
    enum: ['$ USD', '€ EUR', 'MXN $'],
    required: true
  },
  quantity: {
    type: Number,
    enum: [1, 5, 10, 20, 50, 100, 200, 500, 1000],
    required: true
  },
  reward: {
    type: [String],
    required: true
  }
}, {
  timestamps: true
})

paymentSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      idea: this.idea.view(full),
      title: this.title,
      coins: this.coins,
      currency: this.currency,
      quantity: this.quantity,
      reward: this.reward,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Payment', paymentSchema)

export const schema = model.schema
export default model
