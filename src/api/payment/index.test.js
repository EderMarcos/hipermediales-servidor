import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Payment } from '.'

const app = () => express(routes)

let userSession, payment

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  payment = await Payment.create({})
})

test('POST /payments 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: userSession, quantity: 'test', idea: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.quantity).toEqual('test')
  expect(body.idea).toEqual('test')
})

test('POST /payments 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /payments 200 (user)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /payments 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /payments/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`/${payment.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(payment.id)
})

test('GET /payments/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${payment.id}`)
  expect(status).toBe(401)
})

test('GET /payments/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /payments/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${payment.id}`)
    .send({ access_token: userSession, quantity: 'test', idea: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(payment.id)
  expect(body.quantity).toEqual('test')
  expect(body.idea).toEqual('test')
})

test('PUT /payments/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${payment.id}`)
  expect(status).toBe(401)
})

test('PUT /payments/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, quantity: 'test', idea: 'test' })
  expect(status).toBe(404)
})

test('DELETE /payments/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${payment.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /payments/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${payment.id}`)
  expect(status).toBe(401)
})

test('DELETE /payments/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
