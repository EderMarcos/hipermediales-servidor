import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, showByIdea, update, destroy } from './controller'
import { schema } from './model'
export Payment, { schema } from './model'

const router = new Router()
const { idea, title, coins, currency, quantity, reward } = schema.tree

/**
 * @api {post} /payments Create payment
 * @apiName CreatePayment
 * @apiGroup Payment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam quantity Payment's quantity.
 * @apiParam idea Payment's idea.
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ idea, title, coins, currency, quantity, reward }),
  create)

/**
 * @api {get} /payments Retrieve payments
 * @apiName RetrievePayments
 * @apiGroup Payment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} payments List of payments.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /payments/:id Retrieve payment
 * @apiName RetrievePayment
 * @apiGroup Payment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {get} /payments/:idea Retrieve payment
 * @apiName RetrievePayment
 * @apiGroup Payment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 * @apiError 401 user access only.
 */
router.get('/idea/:idea',
  token({ required: true }),
  showByIdea)

/**
 * @api {put} /payments/:id Update payment
 * @apiName UpdatePayment
 * @apiGroup Payment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam quantity Payment's quantity.
 * @apiParam idea Payment's idea.
 * @apiSuccess {Object} payment Payment's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Payment not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ idea, title, coins, currency, quantity, reward }),
  update)

/**
 * @api {delete} /payments/:id Delete payment
 * @apiName DeletePayment
 * @apiGroup Payment
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Payment not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
