import mongoose, { Schema } from 'mongoose'

const sponsorSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  idea: {
    type: Schema.ObjectId,
    ref: 'Idea',
    required: true
  },
  payment: {
    type: Schema.ObjectId,
    ref: 'Payment',
    required: true
  },
  reward: {
    type: String
  }
}, {
  timestamps: true
})

sponsorSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      idea: this.idead.view(full),
      payment: this.paymentd.view(full),
      reward: this.reward,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Sponsor', sponsorSchema)

export const schema = model.schema
export default model
