import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Sponsor } from '.'

const app = () => express(routes)

let userSession, sponsor

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  sponsor = await Sponsor.create({})
})

test('POST /sponsors 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: userSession, reward: 'test', idea: 'test', user: 'test', payment: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.reward).toEqual('test')
  expect(body.idea).toEqual('test')
  expect(body.user).toEqual('test')
  expect(body.payment).toEqual('test')
})

test('POST /sponsors 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /sponsors 200 (user)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /sponsors 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /sponsors/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`/${sponsor.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(sponsor.id)
})

test('GET /sponsors/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${sponsor.id}`)
  expect(status).toBe(401)
})

test('GET /sponsors/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /sponsors/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${sponsor.id}`)
    .send({ access_token: userSession, reward: 'test', idea: 'test', user: 'test', payment: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(sponsor.id)
  expect(body.reward).toEqual('test')
  expect(body.idea).toEqual('test')
  expect(body.user).toEqual('test')
  expect(body.payment).toEqual('test')
})

test('PUT /sponsors/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${sponsor.id}`)
  expect(status).toBe(401)
})

test('PUT /sponsors/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, reward: 'test', idea: 'test', user: 'test', payment: 'test' })
  expect(status).toBe(404)
})

test('DELETE /sponsors/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${sponsor.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /sponsors/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${sponsor.id}`)
  expect(status).toBe(401)
})

test('DELETE /sponsors/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
