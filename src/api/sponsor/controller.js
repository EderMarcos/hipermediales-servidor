import { success, notFound } from '../../services/response/'
import { Sponsor } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Sponsor.create({ ...body })
    .then((sponsor) => sponsor.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Sponsor.count()
  .then(count => {
    return Sponsor.find(query, select, cursor)
    .populate('idea', 'user', 'payment')
    .then(sponsors => ({
      rows: sponsors.map((sponsor) => sponsor.view()),
      count
    }))
  })
  .then(success(res))
  .catch(next)

export const show = ({ params }, res, next) =>
  Sponsor.findById(params.id)
    .populate('idea', 'user', 'payment')
    .then(notFound(res))
    .then((sponsor) => sponsor ? sponsor.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Sponsor.findById(params.id)
    .populate('idea', 'user', 'payment')
    .then(notFound(res))
    .then((sponsor) => sponsor ? Object.assign(sponsor, body).save() : null)
    .then((sponsor) => sponsor ? sponsor.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Sponsor.findById(params.id)
    .then(notFound(res))
    .then((sponsor) => sponsor ? sponsor.remove() : null)
    .then(success(res, 204))
    .catch(next)
