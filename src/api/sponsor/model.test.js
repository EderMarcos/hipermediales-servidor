import { Sponsor } from '.'

let sponsor

beforeEach(async () => {
  sponsor = await Sponsor.create({ reward: 'test', idea: 'test', user: 'test', payment: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = sponsor.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(sponsor.id)
    expect(view.reward).toBe(sponsor.reward)
    expect(view.idea).toBe(sponsor.idea)
    expect(view.user).toBe(sponsor.user)
    expect(view.payment).toBe(sponsor.payment)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = sponsor.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(sponsor.id)
    expect(view.reward).toBe(sponsor.reward)
    expect(view.idea).toBe(sponsor.idea)
    expect(view.user).toBe(sponsor.user)
    expect(view.payment).toBe(sponsor.payment)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
