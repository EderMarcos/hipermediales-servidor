import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Sponsor, { schema } from './model'

const router = new Router()
const { user, idea, payment, reward } = schema.tree

/**
 * @api {post} /sponsors Create sponsor
 * @apiName CreateSponsor
 * @apiGroup Sponsor
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam reward Sponsor's reward.
 * @apiParam idea Sponsor's idea.
 * @apiParam user Sponsor's user.
 * @apiParam payment Sponsor's payment.
 * @apiSuccess {Object} sponsor Sponsor's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Sponsor not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ user, idea, payment, reward }),
  create)

/**
 * @api {get} /sponsors Retrieve sponsors
 * @apiName RetrieveSponsors
 * @apiGroup Sponsor
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} sponsors List of sponsors.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /sponsors/:id Retrieve sponsor
 * @apiName RetrieveSponsor
 * @apiGroup Sponsor
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} sponsor Sponsor's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Sponsor not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /sponsors/:id Update sponsor
 * @apiName UpdateSponsor
 * @apiGroup Sponsor
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam reward Sponsor's reward.
 * @apiParam idea Sponsor's idea.
 * @apiParam user Sponsor's user.
 * @apiParam payment Sponsor's payment.
 * @apiSuccess {Object} sponsor Sponsor's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Sponsor not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ user, idea, payment, reward }),
  update)

/**
 * @api {delete} /sponsors/:id Delete sponsor
 * @apiName DeleteSponsor
 * @apiGroup Sponsor
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Sponsor not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
