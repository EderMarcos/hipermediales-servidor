import mongoose, { Schema } from 'mongoose'

const trendingTopicSchema = new Schema({
  idea: {
    type: String
  },
  month: {
    type: String
  },
  week: {
    type: String
  }
}, {
  timestamps: true
})

trendingTopicSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      idea: this.idea,
      month: this.month,
      week: this.week,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('TrendingTopic', trendingTopicSchema)

export const schema = model.schema
export default model
