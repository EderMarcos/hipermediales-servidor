import { TrendingTopic } from '.'

let trendingTopic

beforeEach(async () => {
  trendingTopic = await TrendingTopic.create({ idea: 'test', month: 'test', week: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = trendingTopic.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(trendingTopic.id)
    expect(view.idea).toBe(trendingTopic.idea)
    expect(view.month).toBe(trendingTopic.month)
    expect(view.week).toBe(trendingTopic.week)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = trendingTopic.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(trendingTopic.id)
    expect(view.idea).toBe(trendingTopic.idea)
    expect(view.month).toBe(trendingTopic.month)
    expect(view.week).toBe(trendingTopic.week)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
