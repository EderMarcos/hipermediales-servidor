import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export TrendingTopic, { schema } from './model'

const router = new Router()
const { idea, month, week } = schema.tree

/**
 * @api {post} /trending-topics Create trending topic
 * @apiName CreateTrendingTopic
 * @apiGroup TrendingTopic
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam idea Trending topic's idea.
 * @apiParam month Trending topic's month.
 * @apiParam week Trending topic's week.
 * @apiSuccess {Object} trendingTopic Trending topic's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Trending topic not found.
 * @apiError 401 admin access only.
 */
router.post('/',
  token({ required: true, roles: ['admin'] }),
  body({ idea, month, week }),
  create)

/**
 * @api {get} /trending-topics Retrieve trending topics
 * @apiName RetrieveTrendingTopics
 * @apiGroup TrendingTopic
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} trendingTopics List of trending topics.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 admin access only.
 */
router.get('/',
  token({ required: true, roles: ['admin'] }),
  query(),
  index)

/**
 * @api {get} /trending-topics/:id Retrieve trending topic
 * @apiName RetrieveTrendingTopic
 * @apiGroup TrendingTopic
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess {Object} trendingTopic Trending topic's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Trending topic not found.
 * @apiError 401 admin access only.
 */
router.get('/:id',
  token({ required: true, roles: ['admin'] }),
  show)

/**
 * @api {put} /trending-topics/:id Update trending topic
 * @apiName UpdateTrendingTopic
 * @apiGroup TrendingTopic
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiParam idea Trending topic's idea.
 * @apiParam month Trending topic's month.
 * @apiParam week Trending topic's week.
 * @apiSuccess {Object} trendingTopic Trending topic's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Trending topic not found.
 * @apiError 401 admin access only.
 */
router.put('/:id',
  token({ required: true, roles: ['admin'] }),
  body({ idea, month, week }),
  update)

/**
 * @api {delete} /trending-topics/:id Delete trending topic
 * @apiName DeleteTrendingTopic
 * @apiGroup TrendingTopic
 * @apiPermission admin
 * @apiParam {String} access_token admin access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Trending topic not found.
 * @apiError 401 admin access only.
 */
router.delete('/:id',
  token({ required: true, roles: ['admin'] }),
  destroy)

export default router
