import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { TrendingTopic } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  TrendingTopic.create(body)
    .then((trendingTopic) => trendingTopic.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  TrendingTopic.find(query, select, cursor)
    .then((trendingTopics) => trendingTopics.map((trendingTopic) => trendingTopic.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  TrendingTopic.findById(params.id)
    .then(notFound(res))
    .then((trendingTopic) => trendingTopic ? trendingTopic.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  TrendingTopic.findById(params.id)
    .then(notFound(res))
    .then((trendingTopic) => trendingTopic ? _.merge(trendingTopic, body).save() : null)
    .then((trendingTopic) => trendingTopic ? trendingTopic.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  TrendingTopic.findById(params.id)
    .then(notFound(res))
    .then((trendingTopic) => trendingTopic ? trendingTopic.remove() : null)
    .then(success(res, 204))
    .catch(next)
