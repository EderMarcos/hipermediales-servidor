import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { TrendingTopic } from '.'

const app = () => express(routes)

let userSession, adminSession, trendingTopic

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  const admin = await User.create({ email: 'c@c.com', password: '123456', role: 'admin' })
  userSession = signSync(user.id)
  adminSession = signSync(admin.id)
  trendingTopic = await TrendingTopic.create({})
})

test('POST /trending-topics 201 (admin)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: adminSession, idea: 'test', month: 'test', week: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.idea).toEqual('test')
  expect(body.month).toEqual('test')
  expect(body.week).toEqual('test')
})

test('POST /trending-topics 401 (user)', async () => {
  const { status } = await request(app())
    .post('/')
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('POST /trending-topics 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /trending-topics 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /trending-topics 401 (user)', async () => {
  const { status } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /trending-topics 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /trending-topics/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .get(`/${trendingTopic.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(trendingTopic.id)
})

test('GET /trending-topics/:id 401 (user)', async () => {
  const { status } = await request(app())
    .get(`/${trendingTopic.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('GET /trending-topics/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${trendingTopic.id}`)
  expect(status).toBe(401)
})

test('GET /trending-topics/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})

test('PUT /trending-topics/:id 200 (admin)', async () => {
  const { status, body } = await request(app())
    .put(`/${trendingTopic.id}`)
    .send({ access_token: adminSession, idea: 'test', month: 'test', week: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(trendingTopic.id)
  expect(body.idea).toEqual('test')
  expect(body.month).toEqual('test')
  expect(body.week).toEqual('test')
})

test('PUT /trending-topics/:id 401 (user)', async () => {
  const { status } = await request(app())
    .put(`/${trendingTopic.id}`)
    .send({ access_token: userSession })
  expect(status).toBe(401)
})

test('PUT /trending-topics/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${trendingTopic.id}`)
  expect(status).toBe(401)
})

test('PUT /trending-topics/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: adminSession, idea: 'test', month: 'test', week: 'test' })
  expect(status).toBe(404)
})

test('DELETE /trending-topics/:id 204 (admin)', async () => {
  const { status } = await request(app())
    .delete(`/${trendingTopic.id}`)
    .query({ access_token: adminSession })
  expect(status).toBe(204)
})

test('DELETE /trending-topics/:id 401 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${trendingTopic.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(401)
})

test('DELETE /trending-topics/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${trendingTopic.id}`)
  expect(status).toBe(401)
})

test('DELETE /trending-topics/:id 404 (admin)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: adminSession })
  expect(status).toBe(404)
})
