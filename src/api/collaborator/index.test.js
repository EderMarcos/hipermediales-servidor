import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Collaborator } from '.'

const app = () => express(routes)

let userSession, collaborator

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  collaborator = await Collaborator.create({})
})

test('POST /collaborators 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: userSession, descriptions: 'test', type: 'test', idea: 'test', user: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.descriptions).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.idea).toEqual('test')
  expect(body.user).toEqual('test')
})

test('POST /collaborators 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /collaborators 200 (user)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /collaborators 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /collaborators/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`/${collaborator.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(collaborator.id)
})

test('GET /collaborators/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${collaborator.id}`)
  expect(status).toBe(401)
})

test('GET /collaborators/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /collaborators/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${collaborator.id}`)
    .send({ access_token: userSession, descriptions: 'test', type: 'test', idea: 'test', user: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(collaborator.id)
  expect(body.descriptions).toEqual('test')
  expect(body.type).toEqual('test')
  expect(body.idea).toEqual('test')
  expect(body.user).toEqual('test')
})

test('PUT /collaborators/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${collaborator.id}`)
  expect(status).toBe(401)
})

test('PUT /collaborators/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, descriptions: 'test', type: 'test', idea: 'test', user: 'test' })
  expect(status).toBe(404)
})

test('DELETE /collaborators/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${collaborator.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /collaborators/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${collaborator.id}`)
  expect(status).toBe(401)
})

test('DELETE /collaborators/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
