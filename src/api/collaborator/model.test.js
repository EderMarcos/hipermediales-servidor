import { Collaborator } from '.'

let collaborator

beforeEach(async () => {
  collaborator = await Collaborator.create({ descriptions: 'test', type: 'test', idea: 'test', user: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = collaborator.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(collaborator.id)
    expect(view.descriptions).toBe(collaborator.descriptions)
    expect(view.type).toBe(collaborator.type)
    expect(view.idea).toBe(collaborator.idea)
    expect(view.user).toBe(collaborator.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = collaborator.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(collaborator.id)
    expect(view.descriptions).toBe(collaborator.descriptions)
    expect(view.type).toBe(collaborator.type)
    expect(view.idea).toBe(collaborator.idea)
    expect(view.user).toBe(collaborator.user)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
