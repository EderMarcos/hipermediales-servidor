import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Collaborator, { schema } from './model'

const router = new Router()
const { descriptions, type } = schema.tree

/**
 * @api {post} /collaborators Create collaborator
 * @apiName CreateCollaborator
 * @apiGroup Collaborator
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam descriptions Collaborator's descriptions.
 * @apiParam type Collaborator's type.
 * @apiSuccess {Object} collaborator Collaborator's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Collaborator not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ descriptions, type }),
  create)

/**
 * @api {get} /collaborators Retrieve collaborators
 * @apiName RetrieveCollaborators
 * @apiGroup Collaborator
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} collaborators List of collaborators.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /collaborators/:id Retrieve collaborator
 * @apiName RetrieveCollaborator
 * @apiGroup Collaborator
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} collaborator Collaborator's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Collaborator not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /collaborators/:id Update collaborator
 * @apiName UpdateCollaborator
 * @apiGroup Collaborator
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam descriptions Collaborator's descriptions.
 * @apiParam type Collaborator's type.
 * @apiParam idea Collaborator's idea.
 * @apiParam user Collaborator's user.
 * @apiSuccess {Object} collaborator Collaborator's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Collaborator not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ descriptions, type }),
  update)

/**
 * @api {delete} /collaborators/:id Delete collaborator
 * @apiName DeleteCollaborator
 * @apiGroup Collaborator
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Collaborator not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
