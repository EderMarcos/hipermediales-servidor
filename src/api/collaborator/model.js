import mongoose, { Schema } from 'mongoose'

const collaboratorSchema = new Schema({
  description: {
    type: String,
    required: true,
    trim: true
  },
  type: {
    type: String,
    required: true,
    trim: true
  }
}, {
  timestamps: true
})


collaboratorSchema.virtual('user', {
    ref : 'User',
    localField : '_id',
    foreignField : 'collaborator'
});

collaboratorSchema.virtual('idea', {
    ref : 'Idea',
    localField : '_id',
    foreignField : 'collaborator'
});


collaboratorSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      description: this.description,
      type: this.type,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Collaborator', collaboratorSchema)

export const schema = model.schema
export default model
