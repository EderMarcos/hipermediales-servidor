import { success, notFound } from '../../services/response/'
import { Collaborator } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Collaborator.create({ ...body })
    .then((collaborator) => collaborator.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Collaborator.count()
  .then(count => {
    return Collaborator.find(query, select, cursor)
      .then(collaborators => ({
        rows: collaborators.map(collaborator => collaborator.view()),
        count
      }))
  })
  .then(success(res))
  .then(next)

export const show = ({ params }, res, next) =>
  Collaborator.findById(params.id)
    .populate('user', 'idea')
    .then(notFound(res))
    .then((collaborator) => collaborator ? collaborator.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Collaborator.findById(params.id)
    .then(notFound(res))
    .then((collaborator) => collaborator ? Object.assign(collaborator, body).save() : null)
    .then((collaborator) => collaborator ? collaborator.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Collaborator.findById(params.id)
    .then(notFound(res))
    .then((collaborator) => collaborator ? collaborator.remove() : null)
    .then(success(res, 204))
    .catch(next)
