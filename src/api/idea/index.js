import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Idea, { schema } from './model'

const router = new Router()
const { title, category, description, pictureUrl, movieUrl, deadline, raisedMoney, goalMoney, statusValid, completed, owner } = schema.tree

/**
 * @api {post} /ideas Create idea
 * @apiName CreateIdea
 * @apiGroup Idea
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam title Idea's title.
 * @apiParam category Idea's category.
 * @apiParam description Idea's description.
 * @apiParam pictureUrl Idea's pictureUrl.
 * @apiParam movieUrl Idea's movieUrl.
 * @apiParam deadline Idea's deadline.
 * @apiParam raisedMoney Idea's raisedMoney.
 * @apiParam goalMoney Idea's goalMoney.
 * @apiParam statusValid Idea's statusValid.
 * @apiParam completed Idea's completed.
 * @apiSuccess {Object} idea Idea's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Idea not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ title, category, description, pictureUrl, movieUrl, deadline, raisedMoney, goalMoney, statusValid, completed, owner }),
  create)

/**
 * @api {get} /ideas Retrieve ideas
 * @apiName RetrieveIdeas
 * @apiGroup Idea
 * @apiUse listParams
 * @apiSuccess {Object[]} ideas List of ideas.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /ideas/:id Retrieve idea
 * @apiName RetrieveIdea
 * @apiGroup Idea
 * @apiSuccess {Object} idea Idea's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Idea not found.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /ideas/:id Update idea
 * @apiName UpdateIdea
 * @apiGroup Idea
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam title Idea's title.
 * @apiParam category Idea's category.
 * @apiParam description Idea's description.
 * @apiParam pictureUrl Idea's pictureUrl.
 * @apiParam movieUrl Idea's movieUrl.
 * @apiParam deadline Idea's deadline.
 * @apiParam raisedMoney Idea's raisedMoney.
 * @apiParam goalMoney Idea's goalMoney.
 * @apiParam statusValid Idea's statusValid.
 * @apiParam completed Idea's completed.
 * @apiSuccess {Object} idea Idea's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Idea not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ title, category, description, pictureUrl, movieUrl, deadline, raisedMoney, goalMoney, statusValid, completed, owner }),
  update)

/**
 * @api {delete} /ideas/:id Delete idea
 * @apiName DeleteIdea
 * @apiGroup Idea
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Idea not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
