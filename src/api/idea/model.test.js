import { Idea } from '.'

let idea

beforeEach(async () => {
  idea = await Idea.create({ title: 'test', category: 'test', description: 'test', pictureUrl: 'test', movieUrl: 'test', deadline: 'test', raisedMoney: 'test', goalMoney: 'test', statusValid: 'test', completed: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = idea.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(idea.id)
    expect(view.title).toBe(idea.title)
    expect(view.category).toBe(idea.category)
    expect(view.description).toBe(idea.description)
    expect(view.pictureUrl).toBe(idea.pictureUrl)
    expect(view.movieUrl).toBe(idea.movieUrl)
    expect(view.deadline).toBe(idea.deadline)
    expect(view.raisedMoney).toBe(idea.raisedMoney)
    expect(view.goalMoney).toBe(idea.goalMoney)
    expect(view.statusValid).toBe(idea.statusValid)
    expect(view.completed).toBe(idea.completed)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = idea.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(idea.id)
    expect(view.title).toBe(idea.title)
    expect(view.category).toBe(idea.category)
    expect(view.description).toBe(idea.description)
    expect(view.pictureUrl).toBe(idea.pictureUrl)
    expect(view.movieUrl).toBe(idea.movieUrl)
    expect(view.deadline).toBe(idea.deadline)
    expect(view.raisedMoney).toBe(idea.raisedMoney)
    expect(view.goalMoney).toBe(idea.goalMoney)
    expect(view.statusValid).toBe(idea.statusValid)
    expect(view.completed).toBe(idea.completed)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
