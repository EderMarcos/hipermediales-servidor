import { success, notFound } from '../../services/response/'
import { Idea } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Idea.create(body)
    .then((idea) => idea.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Idea.count()
  .then(count => {
    return Idea.find(query, select, cursor)
    .populate('owner')
    .then(ideas => ({
      rows: ideas.map(idea => idea.view()),
      count
    }))
  })
  .then(success(res))
  .then(next)

export const show = ({ params }, res, next) =>
  Idea.findById(params.id)
    .populate('owner')
    .then(notFound(res))
    .then((idea) => idea ? idea.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Idea.findById(params.id)
    .populate('owner')
    .then(notFound(res))
    .then((idea) => idea ? Object.assign(idea, body).save() : null)
    .then((idea) => idea ? idea.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Idea.findById(params.id)
    .then(notFound(res))
    .then((idea) => idea ? idea.remove() : null)
    .then(success(res, 204))
    .catch(next)
