import mongoose, { Schema } from 'mongoose'

const categories = ['Música', 'Ciencia', 'Deportes', 'Software', 'Solidarios', 'Medio Ambiente', 'Fotografía', 'Cine y Animación', 'Pintura y Cómic', 'Varios', 'Universidad', 'Libros', 'Teatro y Danza', 'Eventos', 'Videojuegos', 'Diseño', 'Tecnología', 'Desarrolladores', 'Social', 'Cultural', 'Investigación', 'Divulgación', 'Docencia', 'Alimentación', 'Libros Tandai']

const ideaSchema = new Schema({
  owner: {
    type: Schema.ObjectId,
    ref: 'Owner',
    required: true
  },
  title: {
    type: String,
    index: true,
    trim: true,
    required: true
  },
  objetive: {
    type: String,
    trim: true,
    required: true
  },
  country: {
    type: String,
    index: true,
    trim: true,
    required: true
  },
  category: {
    type: String,
    index: true,
    enum: categories,
    required: true
  },
  description: {
    type: String,
    trim: true,
    required: true
  },
  pictureUrl: {
    type: String,
    trim: true,
    required: true
  },
  funded: {
    type: String,
    required: true,
    enum: ['si', 'no']
  },
  deadline: {
    type: String,
    enum: ['6 meses', '12 meses', '18 meses', '24 meses', '36 meses'],
    required: true
  },
  raisedMoney: {
    type: Number,
    trim: true,
  },
  goalMoney: {
    type: Number,
    trim: true,
    required: true
  },
  statusValid: {
    type: String,
    enum: ['approved', 'in process', 'rejected'],
    required: true
  },
  completed: {
    type: String,
    enum: ['done', 'failure', 'in process']
  }
}, {
  timestamps: true
})

ideaSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      owner: this.owner.view(full),
      title: this.title,
      objetive: this.objetive,
      country: this.country,
      category: this.category,
      description: this.description,
      pictureUrl: this.pictureUrl,
      funded: this.funded,
      deadline: this.deadline,
      raisedMoney: this.raisedMoney,
      goalMoney: this.goalMoney,
      statusValid: this.statusValid,
      completed: this.completed,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Idea', ideaSchema)

export const schema = model.schema
export default model
