import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Idea } from '.'

const app = () => express(routes)

let userSession, idea

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  idea = await Idea.create({})
})

test('POST /ideas 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: userSession, title: 'test', category: 'test', description: 'test', pictureUrl: 'test', movieUrl: 'test', deadline: 'test', raisedMoney: 'test', goalMoney: 'test', statusValid: 'test', completed: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.title).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.pictureUrl).toEqual('test')
  expect(body.movieUrl).toEqual('test')
  expect(body.deadline).toEqual('test')
  expect(body.raisedMoney).toEqual('test')
  expect(body.goalMoney).toEqual('test')
  expect(body.statusValid).toEqual('test')
  expect(body.completed).toEqual('test')
})

test('POST /ideas 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /ideas 200', async () => {
  const { status, body } = await request(app())
    .get('/')
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /ideas/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`/${idea.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(idea.id)
})

test('GET /ideas/:id 404', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /ideas/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${idea.id}`)
    .send({ access_token: userSession, title: 'test', category: 'test', description: 'test', pictureUrl: 'test', movieUrl: 'test', deadline: 'test', raisedMoney: 'test', goalMoney: 'test', statusValid: 'test', completed: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(idea.id)
  expect(body.title).toEqual('test')
  expect(body.category).toEqual('test')
  expect(body.description).toEqual('test')
  expect(body.pictureUrl).toEqual('test')
  expect(body.movieUrl).toEqual('test')
  expect(body.deadline).toEqual('test')
  expect(body.raisedMoney).toEqual('test')
  expect(body.goalMoney).toEqual('test')
  expect(body.statusValid).toEqual('test')
  expect(body.completed).toEqual('test')
})

test('PUT /ideas/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${idea.id}`)
  expect(status).toBe(401)
})

test('PUT /ideas/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, title: 'test', category: 'test', description: 'test', pictureUrl: 'test', movieUrl: 'test', deadline: 'test', raisedMoney: 'test', goalMoney: 'test', statusValid: 'test', completed: 'test' })
  expect(status).toBe(404)
})

test('DELETE /ideas/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${idea.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /ideas/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${idea.id}`)
  expect(status).toBe(401)
})

test('DELETE /ideas/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
