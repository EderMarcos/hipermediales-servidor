import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Owner, { schema } from './model'

const router = new Router()
const { business, sector, type, yearOfCreation, bankAccount, facebook, twitter, user } = schema.tree

/**
 * @api {post} /owners Create owner
 * @apiName CreateOwner
 * @apiGroup Owner
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam bankAccount Owner's bankAccount.
 * @apiParam facebook Owner's facebook.
 * @apiParam twitter Owner's twitter.
 * @apiSuccess {Object} owner Owner's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Owner not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ business, sector, type, yearOfCreation, bankAccount, facebook, twitter, user }),
  create)

/**
 * @api {get} /owners Retrieve owners
 * @apiName RetrieveOwners
 * @apiGroup Owner
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} owners List of owners.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /owners/:id Retrieve owner
 * @apiName RetrieveOwner
 * @apiGroup Owner
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} owner Owner's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Owner not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /owners/:id Update owner
 * @apiName UpdateOwner
 * @apiGroup Owner
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam bankAccount Owner's bankAccount.
 * @apiParam facebook Owner's facebook.
 * @apiParam twitter Owner's twitter.
 * @apiSuccess {Object} owner Owner's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Owner not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ business, sector, type, yearOfCreation, bankAccount, facebook, twitter, user }),
  update)

/**
 * @api {delete} /owners/:id Delete owner
 * @apiName DeleteOwner
 * @apiGroup Owner
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Owner not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
