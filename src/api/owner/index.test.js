import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Owner } from '.'

const app = () => express(routes)

let userSession, owner

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  owner = await Owner.create({})
})

test('POST /owners 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: userSession, bankAccount: 'test', facebook: 'test', twitter: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.bankAccount).toEqual('test')
  expect(body.facebook).toEqual('test')
  expect(body.twitter).toEqual('test')
})

test('POST /owners 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /owners 200 (user)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /owners 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /owners/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`/${owner.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(owner.id)
})

test('GET /owners/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${owner.id}`)
  expect(status).toBe(401)
})

test('GET /owners/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /owners/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${owner.id}`)
    .send({ access_token: userSession, bankAccount: 'test', facebook: 'test', twitter: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(owner.id)
  expect(body.bankAccount).toEqual('test')
  expect(body.facebook).toEqual('test')
  expect(body.twitter).toEqual('test')
})

test('PUT /owners/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${owner.id}`)
  expect(status).toBe(401)
})

test('PUT /owners/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, bankAccount: 'test', facebook: 'test', twitter: 'test' })
  expect(status).toBe(404)
})

test('DELETE /owners/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${owner.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /owners/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${owner.id}`)
  expect(status).toBe(401)
})

test('DELETE /owners/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
