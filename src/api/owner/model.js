import mongoose, { Schema } from 'mongoose'

const ownerSchema = new Schema({
  user: {
    type: Schema.ObjectId,
    ref: 'User',
    required: true
  },
  business: {
    type: String,
    trim: true
  },
  sector: {
    type: String,
    trim: true
  },
  type: {
    type: String,
    trim: true
  },
  yearOfCreation: {
    type: String,
    trim: true
  },
  bankAccount: {
    type: String,
    maxlength: 16,
    trim: true
  },
  facebook: {
    type: String,
    trim: true
  },
  twitter: {
    type: String,
    trim: true
  }
}, {
  timestamps: true
})

ownerSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      user: this.user.view(full),
      business: this.business,
      sector: this.sectorser,
      type: this.type,
      yearOfCreation: this.yearOfCreation,
      bankAccount: this.bankAccount,
      facebook: this.facebook,
      twitter: this.twitter,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Owner', ownerSchema)

export const schema = model.schema
export default model
