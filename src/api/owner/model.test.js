import { Owner } from '.'

let owner

beforeEach(async () => {
  owner = await Owner.create({ bankAccount: 'test', facebook: 'test', twitter: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = owner.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(owner.id)
    expect(view.bankAccount).toBe(owner.bankAccount)
    expect(view.facebook).toBe(owner.facebook)
    expect(view.twitter).toBe(owner.twitter)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = owner.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(owner.id)
    expect(view.bankAccount).toBe(owner.bankAccount)
    expect(view.facebook).toBe(owner.facebook)
    expect(view.twitter).toBe(owner.twitter)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
