import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { token } from '../../services/passport'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export Icome, { schema } from './model'

const router = new Router()
const { payment, quantity } = schema.tree

/**
 * @api {post} /icomes Create icome
 * @apiName CreateIcome
 * @apiGroup Icome
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam quantity Icome's quantity.
 * @apiParam payment Icome's payment.
 * @apiSuccess {Object} icome Icome's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Icome not found.
 * @apiError 401 user access only.
 */
router.post('/',
  token({ required: true }),
  body({ payment, quantity }),
  create)

/**
 * @api {get} /icomes Retrieve icomes
 * @apiName RetrieveIcomes
 * @apiGroup Icome
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiUse listParams
 * @apiSuccess {Object[]} icomes List of icomes.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 401 user access only.
 */
router.get('/',
  token({ required: true }),
  query(),
  index)

/**
 * @api {get} /icomes/:id Retrieve icome
 * @apiName RetrieveIcome
 * @apiGroup Icome
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess {Object} icome Icome's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Icome not found.
 * @apiError 401 user access only.
 */
router.get('/:id',
  token({ required: true }),
  show)

/**
 * @api {put} /icomes/:id Update icome
 * @apiName UpdateIcome
 * @apiGroup Icome
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiParam quantity Icome's quantity.
 * @apiParam payment Icome's payment.
 * @apiSuccess {Object} icome Icome's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Icome not found.
 * @apiError 401 user access only.
 */
router.put('/:id',
  token({ required: true }),
  body({ payment, quantity }),
  update)

/**
 * @api {delete} /icomes/:id Delete icome
 * @apiName DeleteIcome
 * @apiGroup Icome
 * @apiPermission user
 * @apiParam {String} access_token user access token.
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Icome not found.
 * @apiError 401 user access only.
 */
router.delete('/:id',
  token({ required: true }),
  destroy)

export default router
