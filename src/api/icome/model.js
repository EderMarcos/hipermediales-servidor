import mongoose, { Schema } from 'mongoose'

const icomeSchema = new Schema({
  payment: {
    type: Schema.ObjectId,
    ref: 'Payment',
    required: true
  },
  quantity: {
    type: Number,
    trim: true
  }
}, {
  timestamps: true
})


icomeSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      payment: this.payment.view(full),
      quantity: this.quantity,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Icome', icomeSchema)

export const schema = model.schema
export default model
