import request from 'supertest-as-promised'
import { signSync } from '../../services/jwt'
import express from '../../services/express'
import { User } from '../user'
import routes, { Icome } from '.'

const app = () => express(routes)

let userSession, icome

beforeEach(async () => {
  const user = await User.create({ email: 'a@a.com', password: '123456' })
  userSession = signSync(user.id)
  icome = await Icome.create({})
})

test('POST /icomes 201 (user)', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ access_token: userSession, quantity: 'test', payment: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.quantity).toEqual('test')
  expect(body.payment).toEqual('test')
})

test('POST /icomes 401', async () => {
  const { status } = await request(app())
    .post('/')
  expect(status).toBe(401)
})

test('GET /icomes 200 (user)', async () => {
  const { status, body } = await request(app())
    .get('/')
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /icomes 401', async () => {
  const { status } = await request(app())
    .get('/')
  expect(status).toBe(401)
})

test('GET /icomes/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .get(`/${icome.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(icome.id)
})

test('GET /icomes/:id 401', async () => {
  const { status } = await request(app())
    .get(`/${icome.id}`)
  expect(status).toBe(401)
})

test('GET /icomes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})

test('PUT /icomes/:id 200 (user)', async () => {
  const { status, body } = await request(app())
    .put(`/${icome.id}`)
    .send({ access_token: userSession, quantity: 'test', payment: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(icome.id)
  expect(body.quantity).toEqual('test')
  expect(body.payment).toEqual('test')
})

test('PUT /icomes/:id 401', async () => {
  const { status } = await request(app())
    .put(`/${icome.id}`)
  expect(status).toBe(401)
})

test('PUT /icomes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ access_token: userSession, quantity: 'test', payment: 'test' })
  expect(status).toBe(404)
})

test('DELETE /icomes/:id 204 (user)', async () => {
  const { status } = await request(app())
    .delete(`/${icome.id}`)
    .query({ access_token: userSession })
  expect(status).toBe(204)
})

test('DELETE /icomes/:id 401', async () => {
  const { status } = await request(app())
    .delete(`/${icome.id}`)
  expect(status).toBe(401)
})

test('DELETE /icomes/:id 404 (user)', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
    .query({ access_token: userSession })
  expect(status).toBe(404)
})
