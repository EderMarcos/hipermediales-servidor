import { Icome } from '.'

let icome

beforeEach(async () => {
  icome = await Icome.create({ quantity: 'test', payment: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = icome.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(icome.id)
    expect(view.quantity).toBe(icome.quantity)
    expect(view.payment).toBe(icome.payment)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = icome.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(icome.id)
    expect(view.quantity).toBe(icome.quantity)
    expect(view.payment).toBe(icome.payment)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
