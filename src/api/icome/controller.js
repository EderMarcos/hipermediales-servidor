import { success, notFound } from '../../services/response/'
import { Icome } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Icome.create({ ...body })
    .then((icome) => icome.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Icome.count()
  .then(count => {
    return Icome.find(query, select, cursor)
    .populate('payment')
    .then(icomes => ({
      rows: incomes.map(icome => icome.view()),
      count
    }))
  })
  .then(success(res))
  .then(next)

export const show = ({ params }, res, next) =>
  Icome.findById(params.id)
    .populate('payment')
    .then(notFound(res))
    .then((icome) => icome ? icome.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Icome.findById(params.id)
    .populate('payment')
    .then(notFound(res))
    .then((icome) => icome ? Object.assign(icome, body).save() : null)
    .then((icome) => icome ? icome.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Icome.findById(params.id)
    .then(notFound(res))
    .then((icome) => icome ? icome.remove() : null)
    .then(success(res, 204))
    .catch(next)
