import { Router } from 'express'
import auth from './auth'
import user from './user'
import idea from './idea'
import owner from './owner'
import sponsor from './sponsor'
import collaborator from './collaborator'
import icome from './icome'
import payment from './payment'
import trendingTopic from './trending-topic'

const router = new Router()

/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */
router.use('/auth', auth)
router.use('/users', user)
router.use('/ideas', idea)
router.use('/owners', owner)
router.use('/sponsors', sponsor)
router.use('/collaborators', collaborator)
router.use('/icomes', icome)
router.use('/payments', payment)
router.use('/trending-topics', trendingTopic)

export default router
